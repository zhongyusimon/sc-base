# sc-base
spring cloud基础项目demo

# 包含内容
1. discovery: eureka注册中心
2. config-server: config配置中心
3. gateway: zuul网关
4. boot-admin: 监控中心